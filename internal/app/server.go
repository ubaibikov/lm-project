package app

import (
	"github.com/gin-gonic/gin"
	"work/lm/cmd/apiserver"
	"work/lm/store"
)

// Server ...
type Server struct {
	Config *apiserver.Config
	Store  *store.Store
}

//New ...
func New(cfd *apiserver.Config, s *store.Store) *Server {
	return &Server{
		Store:  s,
		Config: cfd,
	}
}

// Start ...
func (s *Server) Start() error {
	router := gin.Default()
	if err := s.configStore(); err != nil {
		return err
	}
	s.configRoute(router)
	return router.Run(s.Config.Port)
}

func (s *Server) configStore() error {
	if err := s.Store.Open(); err != nil {
		return err
	}

	return nil
}

func (s *Server) configRoute(r *gin.Engine) {
	v1 := r.Group("v1")
	{
		v1.POST("/login", s.login)
		v1.POST("/register", s.register)
		v1.GET("/index", userAuth, s.index)
	}
}
