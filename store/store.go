package store

import (
	"database/sql"

	_ "github.com/lib/pq"
	"work/lm/internal/app/repository"
)

// Store ...
type Store struct {
	db        *sql.DB
	ConnectDB string
}

// New ...
func New(connect string) *Store {
	return &Store{
		ConnectDB: connect,
	}
}

// Open ...
func (s *Store) Open() error {
	db, err := sql.Open("postgres", s.ConnectDB)
	if err != nil {
		return err
	}

	if err := db.Ping(); err != nil {
		return err
	}

	s.db = db
	return nil
}

// Close ...
func (s *Store) Close() {
	s.db.Close()
}

// User ...
func (s *Store) User() *repository.UserRepository {
	return &repository.UserRepository{
		DB: s.db,
	}
}
