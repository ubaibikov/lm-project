package main

import (
	"flag"
	"log"

	"work/lm/cmd/apiserver"
	"work/lm/internal/app"
	"work/lm/store"

	"github.com/BurntSushi/toml"
)

var (
	configPath string
)

func init() {
	flag.StringVar(&configPath, "config-path", "./configs/apiserver.toml", "path to config file")
	flag.Parse()
}

func main() {
	config := parceConfig()

	store := store.New(config.DBUrl)
	app := app.New(config, store)
	if err := app.Start(); err != nil {
		panic(err)
	}
}

func parceConfig() *apiserver.Config {
	initConf := apiserver.InitConfig()

	if _, err := toml.DecodeFile(configPath, initConf); err != nil {
		log.Fatal(err.Error())
	}

	return initConf
}
