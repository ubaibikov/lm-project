package apiserver

// Config ...
type Config struct {
	Port  string `toml:"port"`
	DBUrl string `toml:"db_url"`
	Host  string `toml:"host"`
}

//InitConfig ...
func InitConfig() *Config {
	return &Config{}
}
